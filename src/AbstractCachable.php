<?php

namespace Fortress\Eloquent;

use Fortress\Eloquent\Generator\UrlCacheKeyGenerator;
use Illuminate\Contracts\Cache\Repository as CacheInterface;
use Illuminate\Http\Request;

abstract class AbstractCachable
{
    protected QueryBuilderInterface $repository;
    protected ?CacheInterface $cache;
    protected int $ttl;
    protected Request $request;
    protected array $with = [];
    protected array $withCount = [];
    protected array $cacheKeys = [];

    public function __construct(
        QueryBuilderInterface $repository,
        CacheInterface $cache,
        Request $request,
        bool $enabled = true,
        int $ttl = 600
    ) {
        $this->cache = null;
        $this->repository = $repository;
        $this->request = $request;
        $this->ttl = (int) $this->request->query->get('ttl', $ttl);

        if ($enabled) {
            $this->setCache($cache);
        }
    }

    public function addKey(string $key): CachableQueryBuilderInterface
    {
        $this->cacheKeys[] = $key;

        return $this;
    }

    public function addKeys(array $keys): CachableQueryBuilderInterface
    {
        $this->cacheKeys = array_merge($this->cacheKeys, $keys);

        return $this;
    }

    private function setCache(CacheInterface $cache): void
    {
        if (method_exists($cache, 'tags')) {
            $cache = $cache->tags($this->getTable());
        }

        $this->cache = $cache;
    }

    protected function getCacheKey(): string
    {
        $generator = new UrlCacheKeyGenerator();

        return $generator->generate($this->request, $this->cacheKeys);
    }

    abstract protected function getTable(): string;
}
