<?php

namespace Fortress\Eloquent;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CachableQueryBuilder
 * @package Fortress\Eloquent
 */
class CachableQueryBuilder extends AbstractCachable implements CachableQueryBuilderInterface
{
    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->repository->getModel();
    }

    /**
     * @return Builder
     */
    public function getQuery(): Builder
    {
        return $this->repository->getQuery();
    }

    /**
     * @param callable $callable
     *
     * @return QueryBuilderInterface
     */
    public function inject(callable $callable): QueryBuilderInterface
    {
        return $this->repository->inject($callable);
    }

    /**
     * @param $id
     * @param array $columns
     * @return Model
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function find($id, array $columns = ['*']): Model
    {
        if (!$this->cache) {
            return $this->repository->find($id, $columns);
        }

        $this->addKeys($columns);

        return $this->getOrCache($this->getCacheKey(), function () use ($id, $columns) {
            return $this->repository->find($id, $columns);
        });
    }

    /**
     * @param array $columns
     * @return Collection
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function get(array $columns = ['*']): Collection
    {
        if (!$this->cache) {
            return $this->repository->get($columns);
        }

        $this->addKeys($columns);

        return $this->getOrCache($this->getCacheKey(), function () use ($columns) {
            return $this->repository->get($columns);
        });
    }

    /**
     * @param array $columns
     * @return LengthAwarePaginator
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function paginate(array $columns = ['*']): LengthAwarePaginator
    {
        if (!$this->cache) {
            return $this->repository->paginate($columns);
        }

        $this->addKeys($columns);

        return $this->getOrCache($this->getCacheKey(), function () use ($columns) {
            return $this->repository->paginate($columns);
        });
    }

    /**
     * @return int
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function count(): int
    {
        if (!$this->cache) {
            return $this->repository->count();
        }

        return $this->getOrCache($this->getCacheKey(), function () {
            return $this->repository->count();
        });
    }

    /**
     * @param $key
     * @param callable $callable
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected function getOrCache($key, callable $callable)
    {
        $value = $this->cache->get($this->getCacheKey());

        if (!is_null($value)) {
            return $value;
        }

        $value = $callable();

        $this->cache->put($key, $value, $this->ttl);

        return $value;
    }

    /**
     * @return string
     */
    protected function getTable(): string
    {
        return $this->getModel()->getTable();
    }
}
