<?php

namespace Fortress\Eloquent;

interface CachableQueryBuilderInterface extends QueryBuilderInterface
{
    public function addKey(string $key): CachableQueryBuilderInterface;
    public function addKeys(array $keys): CachableQueryBuilderInterface;
}
