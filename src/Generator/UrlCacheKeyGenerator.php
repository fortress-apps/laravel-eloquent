<?php

namespace Fortress\Eloquent\Generator;

use Illuminate\Http\Request;

class UrlCacheKeyGenerator implements FromRequestGeneratorInterface
{
    private string $separator;

    public function __construct(string $separator = '_')
    {
        $this->separator = $separator;
    }

    public function generate(Request $request, array $additional = []): string
    {
        $keyParts = array_merge([
            $request->getScheme(),
            $request->getHost(),
            $request->getPort(),
            $this->getPathInfo($request),
            $request->getMethod(),
            $this->sortQueryString($request)
        ], $additional);

        $cacheKey = implode($this->separator, $keyParts);

        return $this->generateSafeKey($cacheKey);
    }

    private function sortQueryString(Request $request): string
    {
        $queryArray = $request->query->all();

        if (empty($queryArray)) {
            return '';
        }

        $queryArray = $this->sortNestedParameters($queryArray);

        return urldecode(http_build_query($queryArray));
    }

    private function getPathInfo(Request $request): string
    {
        return ltrim($request->getPathInfo(), '/');
    }

    private function generateSafeKey($key): string
    {
        return str_replace(
            ['{', '}', '(', ')', '[', ']', '/', '\\', '@', ':', '?', '&', ';', '=', ','],
            '_',
            $key
        );
    }

    private function sortNestedParameters(array $params): array
    {
        if (is_array($params)) {
            ksort($params);

            foreach ($params as $key => $value) {
                if (is_array($value)) {
                    $params[$key] = $this->sortNestedParameters($value);
                }
            }
        }

        return $params;
    }
}
