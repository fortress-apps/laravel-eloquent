<?php

namespace Fortress\Eloquent\Generator;

use Illuminate\Http\Request;

interface FromRequestGeneratorInterface
{
    public function generate(Request $request): string;
}
