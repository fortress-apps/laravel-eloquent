<?php

namespace Fortress\Eloquent\Filter;

use Fortress\TypeCollection\AbstractGenericCollection;

class FilterList extends AbstractGenericCollection
{
    protected function willAcceptType($value): bool
    {
        return $value instanceof FilterInterface;
    }
}
