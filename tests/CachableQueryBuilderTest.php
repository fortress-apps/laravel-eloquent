<?php

namespace Fortress\Eloquent\Tests;

use Fortress\Eloquent\CachableQueryBuilder;
use Fortress\Eloquent\QueryBuilderInterface;
use Illuminate\Cache\TaggedCache;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Illuminate\Database\Eloquent\Builder;
use PHPUnit\Framework\MockObject\MockObject;
use Illuminate\Contracts\Cache\Repository as Cache;

/**
 * Class CachableQueryBuilderTest
 * @package Fortress\Eloquent\Tests
 * @group CachableQueryBuilderTest
 */
class CachableQueryBuilderTest extends TestCase
{

    /** @var Model|MockObject */
    private $model;

    /** @var Builder|MockObject */
    private $query;

    /** @var QueryBuilderInterface|MockObject */
    private $queryBuilder;

    /** @var Cache|MockObject */
    private $cache;

    /** @var Request|MockObject */
    private $request;

    public function setUp(): void
    {
        parent::setUp();

        $this->model = $this->createMock(Model::class);
        $this->query = $this->createMock(Builder::class);
        $this->queryBuilder = $this->createMock(QueryBuilderInterface::class);
        $this->cache = $this->createMock(Cache::class);
        $this->request = $this->createMock(Request::class);
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    private function mockGetTable()
    {
        $this->queryBuilder->expects($this->any())
            ->method('getModel')
            ->willReturn($this->model);

        $this->model->expects($this->any())
            ->method('getTable')
            ->willReturn('test_table');
    }

    public function testAddKey()
    {
        $request = Request::create('/test?filter[score]=10&include=user,posts');
        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request);

        $this->assertInstanceOf(CachableQueryBuilder::class, $sut->addKey('test'));
    }

    public function testSetKey()
    {
        $request = Request::create('/test?filter[score]=10&include=user,posts');
        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request);

        $this->assertInstanceOf(CachableQueryBuilder::class, $sut->addKey('test'));
    }

    public function testSetCache()
    {
        $this->mockGetTable();

        $request = Request::create('/test?filter[score]=10&include=user,posts');

        $cache = $this->createMock(TaggedCache::class);
        $cache->expects($this->once())
            ->method('tags')
            ->with('test_table')
            ->willReturnSelf();

        new CachableQueryBuilder($this->queryBuilder, $cache, $request);
    }

    public function testGetQueryBuilder()
    {
        $request = Request::create('/test?filter[score]=10&include=user,posts');
        $builder = $this->createMock(Builder::class);

        $this->queryBuilder->expects($this->once())
            ->method('getQuery')
            ->willReturn($builder);

        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request);

        $this->assertInstanceOf(Builder::class, $sut->getQuery());
    }

    public function testInject()
    {
        $callable = function () {
        };
        $request = Request::create('/test?filter[score]=10&include=user,posts');

        $this->queryBuilder->expects($this->once())
            ->method('inject')
            ->with($callable)
            ->willReturnSelf();

        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request);

        $this->assertInstanceOf(QueryBuilderInterface::class, $sut->inject($callable));
    }

    public function testFind()
    {
        $request = Request::create('/test/1');

        $this->mockGetTable();

        $this->cache->expects($this->once())
            ->method('get')
            ->with($this->isType('string'))
            ->willReturn(null);

        $this->queryBuilder->expects($this->once())
            ->method('find')
            ->with(1, ['*'])
            ->willReturn($this->model);

        $this->cache->expects($this->once())
            ->method('put')
            ->with($this->isType('string'), $this->model, 10);

        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request, true, 10);

        $this->assertInstanceOf(Model::class, $sut->find(1));
    }

    public function testFindNoCache()
    {
        $request = Request::create('/test?filter[score]=10&include=user,posts');

        $this->cache->expects($this->never())
            ->method('get');

        $this->queryBuilder->expects($this->once())
            ->method('find')
            ->with(1, ['*'])
            ->willReturn($this->model);

        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request, false, 10);

        $this->assertInstanceOf(Model::class, $sut->find(1));
    }

    public function testFindCacheExists()
    {
        $request = Request::create('/test?filter[score]=10&include=user,posts');
        $this->mockGetTable();

        $this->cache->expects($this->once())
            ->method('get')
            ->with($this->isType('string'))
            ->willReturn($this->model);

        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request, true, 10);

        $this->assertInstanceOf(Model::class, $sut->find(1));
    }

    public function testGet()
    {
        $request = Request::create('/test?filter[score]=10&include=user,posts');
        $collection = new Collection;

        $this->mockGetTable();

        $this->cache->expects($this->once())
            ->method('get')
            ->with($this->isType('string'))
            ->willReturn(null);

        $this->queryBuilder->expects($this->once())
            ->method('get')
            ->with(['*'])
            ->willReturn($collection);

        $this->cache->expects($this->once())
            ->method('put')
            ->with($this->isType('string'), $collection, 10);

        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request, true, 10);

        $this->assertInstanceOf(Collection::class, $sut->get());
    }

    public function testGetNoCache()
    {
        $request = Request::create('/test?filter[score]=10&include=user,posts');
        $collection = new Collection;

        $this->cache->expects($this->never())
            ->method('get');

        $this->queryBuilder->expects($this->once())
            ->method('get')
            ->with(['*'])
            ->willReturn($collection);

        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request, false, 10);

        $this->assertInstanceOf(Collection::class, $sut->get());
    }

    public function testPaginate()
    {
        $request = Request::create('/test?filter[score]=10&include=user,posts');
        $response = $this->createMock(LengthAwarePaginator::class);

        $this->mockGetTable();

        $this->cache->expects($this->once())
            ->method('get')
            ->with($this->isType('string'))
            ->willReturn(null);

        $this->queryBuilder->expects($this->once())
            ->method('paginate')
            ->with(['*'])
            ->willReturn($response);

        $this->cache->expects($this->once())
            ->method('put')
            ->with($this->isType('string'), $response, 10);

        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request, true, 10);

        $this->assertInstanceOf(LengthAwarePaginator::class, $sut->paginate());
    }

    public function testPaginateNoCache()
    {
        $request = Request::create('/test?filter[score]=10&include=user,posts');
        $response = $this->createMock(LengthAwarePaginator::class);

        $this->cache->expects($this->never())
            ->method('get');

        $this->queryBuilder->expects($this->once())
            ->method('paginate')
            ->with(['*'])
            ->willReturn($response);

        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request, false, 10);

        $this->assertInstanceOf(LengthAwarePaginator::class, $sut->paginate());
    }

    public function testCount()
    {
        $request = Request::create('/test?filter[score]=10&include=user,posts');

        $this->mockGetTable();

        $this->cache->expects($this->once())
            ->method('get')
            ->with($this->isType('string'))
            ->willReturn(null);

        $this->queryBuilder->expects($this->once())
            ->method('count')
            ->willReturn(18);

        $this->cache->expects($this->once())
            ->method('put')
            ->with($this->isType('string'), 18, 10);

        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request, true, 10);

        $this->assertEquals(18, $sut->count());
    }

    public function testCountNoCache()
    {
        $request = Request::create('/test?filter[score]=10&include=user,posts');

        $this->cache->expects($this->never())
            ->method('get');

        $this->queryBuilder->expects($this->once())
            ->method('count')
            ->willReturn(18);

        $sut = new CachableQueryBuilder($this->queryBuilder, $this->cache, $request, false, 10);

        $this->assertEquals(18, $sut->count());
    }
}
