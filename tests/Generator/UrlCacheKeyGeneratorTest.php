<?php

namespace Fortress\Eloquent\Tests\Generator;

use Fortress\Eloquent\Generator\UrlCacheKeyGenerator;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;

class UrlCacheKeyGeneratorTest extends TestCase
{
    public function testGeneratesKey()
    {
        $request = Request::create(
            '/url/called',
            Request::METHOD_GET,
            [
                'test' => 'true'
            ]
        );

        $sut = new UrlCacheKeyGenerator();
        $result = $sut->generate($request);

        $this->assertEquals('http_localhost_80_url_called_GET_test_true', $result);
    }

    public function testGeneratesKeyComplexParams()
    {
        $params = [
            'sort' => [
                'title' => 'asc'
            ],
            'filter' => [
                'type' => 1,
                'private' => 0,
            ],
        ];

        $request = Request::create(
            '/url/called',
            Request::METHOD_GET,
            $params
        );

        $expected = 'http_localhost_80_url_called_GET_filter_private__0_filter_type__1_sort_title__asc';

        $sut = new UrlCacheKeyGenerator();
        $result = $sut->generate($request);

        $this->assertEquals($expected, $result);
    }

    public function testGeneratesAdditional()
    {
        $request = Request::create(
            '/url/called',
            Request::METHOD_GET,
            [
                'test' => 'true'
            ]
        );

        $sut = new UrlCacheKeyGenerator();
        $result = $sut->generate($request, [
            'guest'
        ]);

        $this->assertEquals('http_localhost_80_url_called_GET_test_true_guest', $result);
    }
}
